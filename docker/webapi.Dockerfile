# 拉取debian映像檔
FROM debian:11

# 安裝依賴套件(包含python3.9和apache)
RUN apt-get update \
	&& apt-get install -y build-essential vim-tiny \ 
        python3.9-venv python3.9-dev \
        apache2 libapache2-mod-wsgi-py3 \
		libmariadb3 libmariadb-dev \ 
        libldap-2.4-2 libldap2-dev \ 
        libsasl2-dev libsasl2-2 libsasl2-modules libsasl2-modules-db \
        libmariadb-dev-compat \
        vim \
	&& apt-get clean

# 建立python虛擬環境 和 啟動apache
RUN cd /opt \ 
    && python3 -m venv webapi_env

# 將Django專案複製進容器
COPY ["www/webapi", "/var/www/webapi"]

# 將設定檔複製至對應位置
COPY ["docker/build_config/apache/webapi/webapi.conf", "/etc/apache2/sites-enabled/webapi.conf"]
COPY ["docker/build_config", "var/build_config"]

# 因為RUN的默認shell為"/bin/sh"，故要先將他轉為"/bin/bash"，才可以使用source命令
SHELL ["/bin/bash", "-c"]

RUN ln -s /etc/apache2/mods-available/proxy.load /etc/apache2/mods-enabled/ \
    && ln -s /etc/apache2/mods-available/proxy_http.load /etc/apache2/mods-enabled/

# 安裝python套件
RUN source /opt/webapi_env/bin/activate \
    && pip install -r /var/build_config/python/webapi/requirements.txt

# www-data為apache用戶及群組
RUN chown -R www-data:www-data /var/www

ENTRYPOINT ["bash", "/var/build_config/docker/webapi/entrypoint.sh"]
