# 拉取debian映像檔
FROM debian:11

# 安裝依賴套件(包含python3.9和apache)
RUN apt-get update \
	&& apt-get install -y build-essential vim-tiny \ 
        python3.9-venv python3.9-dev \
        apache2 libapache2-mod-wsgi-py3 \
		libmariadb3 libmariadb-dev \ 
        libldap-2.4-2 libldap2-dev \ 
        libsasl2-dev libsasl2-2 libsasl2-modules libsasl2-modules-db \
        libmariadb-dev-compat \
        vim \
	&& apt-get clean

# 建立python虛擬環境 和 啟動apache
RUN cd /opt \ 
    && python3 -m venv webapi_2_env

# 將Django專案複製進容器
COPY ["www/webapi_2", "/var/www/webapi_2"]

# 將設定檔複製至對應位置
COPY ["docker/build_config/apache/webapi_2/webapi_2.conf", "/etc/apache2/sites-enabled/webapi_2.conf"]
COPY ["docker/build_config", "var/build_config"]

# 因為RUN的默認shell為"/bin/sh"，故要先將他轉為"/bin/bash"，才可以使用source命令
SHELL ["/bin/bash", "-c"]

# 安裝python套件
RUN source /opt/webapi_2_env/bin/activate \
    && pip install -r /var/build_config/python/webapi_2/requirements.txt

# www-data為apache用戶及群組
RUN chown -R www-data:www-data /var/www

ENTRYPOINT ["bash", "/var/build_config/docker/webapi_2/entrypoint.sh"]
